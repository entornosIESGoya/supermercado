package es.goya;

public class Cafe extends Producto{
	
	private final double PRECIO_UNITARIO = 4.8;
	
	public Cafe(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Cafe Soluble";
	}

}
