package es.goya;

import java.util.ArrayList;
import java.util.Scanner;

public class Supermercado 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		ArrayList<Producto> productos = new ArrayList<Producto>();
		Scanner scanner = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("Introduzca producto: (0 para salir)");
			System.out.println("1.- Cafe");
			System.out.println("8.- Banana");

			System.out.println("2.- Azucar");



			System.out.println("10.- Tequeños de Queso");


			System.out.println("6.- Chocolate");
			System.out.println("10.- Tequeños de Queso");
			System.out.println("12.- Caviar de Beluga");


			System.out.println("13.- Jamon Iberico de Bellota");


			
			opcion = scanner.nextInt();
			switch(opcion) {
			case 1:
				System.out.println("Dame unidades de cafe:");
				int unidades = scanner.nextInt();
				Cafe cafe = new Cafe(unidades);
				productos.add(cafe);
				break;
				
			case 8:
				System.out.println("Dame unidades de Banana:");
				unidades = scanner.nextInt();
				Banana banana = new Banana(unidades);
				productos.add(banana);
				break;
				
			case 2:
				System.out.println("Dame unidades de azucar:");
				unidades = scanner.nextInt();
				Azucar azucar = new Azucar(unidades);
				productos.add(azucar);
				break;
				
				
			case 3:
				System.out.println("Dame unidades de Morcilla:");
				unidades = scanner.nextInt();
				MorcillaDeBurgos Morcilla = new MorcillaDeBurgos(unidades);
				productos.add(Morcilla);
				break;
				
				

			case 6:
				System.out.println("Dame unidades de chocolate:");
				unidades = scanner.nextInt();
				Chocolate chocolate = new Chocolate(unidades);
				productos.add(chocolate);
				break;
			
			case 10: 
				System.out.println("Dame unidades de tequeños:");
				unidades = scanner.nextInt();
				TequenosDeQueso tequenos = new TequenosDeQueso(unidades);
				productos.add(tequenos);
				break;
				
			case 12:
				System.out.println("Dame unidades de caviar de beluga:");
				unidades = scanner.nextInt();
				CaviarDeBeluga caviarDeBeluga = new CaviarDeBeluga(unidades);
				productos.add(caviarDeBeluga);
				break;

				
			case 13:
				System.out.println("Dame unidades de jamon:");
				unidades = scanner.nextInt();
				JamonIbericoDeBellota jamon = new JamonIbericoDeBellota(unidades);
				productos.add(jamon);
				break;
			}
		} while(opcion!=0);
		double total = 0;

		for(Producto producto:productos) 
		{
			System.out.println("Unidades: " + producto.getUnidades() + " "+ producto.getNombre() + " " + producto.getPrecioTotal());

			total += producto.getPrecioTotal();
		}
		System.out.println("Total tique: " + total + " euros.");
	}

}
