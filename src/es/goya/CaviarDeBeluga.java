package es.goya;

public class CaviarDeBeluga extends Producto{
	
	private final double PRECIO_UNITARIO = 666;
	
	public CaviarDeBeluga(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Caviar de Beluga (белужья икра)";	
	}

}
