package es.goya;

public class JamonIbericoDeBellota extends Producto{
private final double PRECIO_UNITARIO = 500.00;
	
	public JamonIbericoDeBellota(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Jamon Iberico de Bellota (El extremeño)";
	}

}
