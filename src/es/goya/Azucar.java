package es.goya;

public class Azucar extends Producto{
	
	private final double PRECIO_UNITARIO = 1.5;
	
	public Azucar(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Azucar Moreno";
	}

}
