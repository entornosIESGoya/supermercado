package es.goya;

public class Chocolate extends Producto{
	
	private final double PRECIO_UNITARIO = 2.2;
	
	public Chocolate(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Chocolate Jungly";
	}

}
