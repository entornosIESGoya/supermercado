package es.goya;

public class TequenosDeQueso extends Producto{
	
	private final double PRECIO_UNITARIO = 20;
	
	public TequenosDeQueso(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Tequeños de Queso";
	}

}
