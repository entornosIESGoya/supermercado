package es.goya;

public class Banana extends Producto
{
	private final double PRECIO_UNITARIO = 1;
	
	public Banana(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Banana Canaria";
	}
}
