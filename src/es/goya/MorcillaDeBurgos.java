package es.goya;

public class MorcillaDeBurgos extends Producto{
	
	private final double PRECIO_UNITARIO = 8.5;
	
	public MorcillaDeBurgos(int unidades) {
		super.unidades = unidades;
	}
	
	public double getPrecioTotal() {
		return unidades * PRECIO_UNITARIO;
	}
	
	public String getNombre() {
		return "Morcilla de Burgos";
	}
	

}
