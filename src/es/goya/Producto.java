package es.goya;

public abstract class Producto {
	protected int unidades;
	
	public int getUnidades() {
		return unidades;
	}
	
	public abstract String getNombre(); 
	public abstract double getPrecioTotal();
}
